<?php
include "header_admin.php";
?>
<?php 
include 'database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">



        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data User</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
     
            <a href="laporan_transaksi_pdf.php" target="_blank" class="btn btn-primary">Export Data PDf</a></p>
         

             <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kasir</th>
                <th>Tanggal</th>
                <th>Total Bayar</th>
                <th>Jumlah Uang</th>
                <th>Kembalian</th>
              </tr>
            </thead>
              <tbody>
                   <?php
                   error_reporting(0);
            $no = 1;
            foreach($db->tampil_data_transaksi() as $x){
              ?>

                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                    <td><?php echo $x['tanggal']; ?></td>
                  <td><?php echo $x['total_bayar']; ?></td>
                  <td><?php echo $x['jumlah_uang']; ?></td>
                  <td><?php echo $x['kembalian']; ?></td>
                        

                </tr>

          

                 <?php 
            }
            ?>
              </tbody>
             
          </table>
        </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->


  </div><!-- /.row -->
  <!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include "footer_admin.php";
?>