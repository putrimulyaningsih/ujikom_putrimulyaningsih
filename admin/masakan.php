<?php
include "header_admin.php";
?>
<?php 
include 'database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">



        <div class="box">
          <div class="box-header">

            <h3 class="box-title">Data Menu Makanan</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
          <table>
          <tr>
          <td>
            <p align="left"><a href="#masakantambah" data-toggle="modal" class="btn btn-primary">Tambah Data</a></p>
            </td>
            <td>
            <p align="left"><a href="laporan_masakan.php" target="_blank" class="btn btn-primary">Export Data PDf</a></p>
            </td>
            </tr>
            </table>
        
           <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Masakan</th>
                  <th>Harga Masakan</th>
                  <th>Gambar Masakan</th>
                  <th>Kategori Masakan</th>
                   <th>Status Masakan</th>
                  <th>Aksi</th>

                </tr>
              </thead>
             
                <tbody>
                   <?php
              $no = 1;
              foreach($db->tampil_data() as $x){
                $harga= $x['harga'];
                $hasil="Rp".number_format($harga,2,',','.');
             
                ?>

                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $x['nama_masakan']; ?></td>

                    <td><?php echo $hasil; ?></td>
                     <td><img src="../images/<?php echo $x['gambar']; ?>" height='100'></td>
                      <td><?php echo $x['nama_kategori'];?></td>
                                <td>
    
                        <?php
                                            if($x['status_masakan'] == 'Y')
                                            {
                                              ?>
                                            <a href="approve_masakan.php?table=masakan&id_masakan=<?php echo $x['id_masakan']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                            Tersedia
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="approve_masakan.php?table=masakan&id_masakan=<?php echo $x['id_masakan']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Tidak Tersedia
                                            </a>
                                            <?php
                                            }
                                            ?>
                  </td>
                   
                    <td>
                      <a href="" data-toggle="modal" data-target="#myModal<?php echo $x['id_masakan'];?>" class="btn btn-warning">Edit</a> 
                   
                    </td>

                  </tr>
                  <div class="modal" id="myModal<?php echo $x['id_masakan'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Edit Menu</h4>
                  </div>
<?php
include '../login/koneksi.php';
$id = $x['id_masakan']; 
$query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id'");
$r = mysqli_fetch_array($query_edit);
?>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="update_masakan.php?id_masakan=<?php echo $r['id_masakan'];?>" enctype="multipart/form-data" class="form-horizontal form-material"">
                            <div class="form-group">
                              <label for="nama_masakan">Nama Masakan</label>
                              <input type="hidden" name="id_masakan" value="<?php echo $r['id_masakan'];?>">
                              <input type="text" class="form-control" name="nama_masakan" id="nama_masakan" value="<?php echo $r['nama_masakan'];?>" placeholder="Masukan Nama">
                            </div>
                            <div class="form-group">
                              <label for="harga">Harga</label>
                              <input type="integer" class="form-control" name="harga" id="harga" value="<?php echo $r['harga'];?>" placeholder="Masukan Harga">
                            </div> 

                            <div class="form-group">
                              <label for="Gambar">Masukan Gambar</label>
                              <input type="file" class="form-control" name="gambar" value="gambar/<?php echo $r['gambar'];?>">
                            </div>
                                  <div class="form-group">
                                 <label for="id_kategori" >Nama Kategori :</label>
                               
                                    <select name="id_kategori" class="form-control">
                                    <?php     
                                    include"../login/koneksi.php";
                                      $select=mysqli_query($conn, "SELECT * FROM kategori");
                                      while($show=mysqli_fetch_array($select)){
                                    ?>
                                      <option value="<?=$show['id_kategori'];?>" <?=$r['id_kategori']==$show['id_kategori']?'selected':null?>><?=$show['nama_kategori'];?></option>
                                    <?php } ?>
                                  </select>
                               
                                </div>

                                
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                      <?php 
              }
              ?>
                </tbody>
            
            </table>
             <div class="table-responsive">
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->


    </div><!-- /.row -->
    <!-- Main row -->


  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
 <div class="modal" id="masakantambah">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Tambah Menu</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses_tambah_masakan.php" enctype="multipart/form-data" class="form-horizontal form-material"">
                        <div class="box-body">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Nama Masakan</label>
                              <input type="text" class="form-control" name="nama_masakan" id="exampleInputPassword1" placeholder="Masukan Nama" required>
                            </div>
                             
                            <div class="form-group">
                              <label for="exampleInputPassword1">Harga</label>
                              <input type="integer" class="form-control" name="harga" id="exampleInputPassword1" placeholder="Masukan Harga" required>
                            </div>
                                <div class="form-group">
                                    <label for="id_kategori">Nama Kategori :</label>
                                    <select name="id_kategori" class="form-control" required>
                                       <option>Pilih Kategori</option>
                                          <?php     
                                            include"../login/koneksi.php";
                                            $select=mysqli_query($conn, "SELECT * FROM kategori");
                                            while($show=mysqli_fetch_array($select)){
                                          ?>
                                      <option value="<?=$show['id_kategori'];?>"><?=$show['nama_kategori'];?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                            <div class="form-group">
                              <label for="exampleInputFile">Masukan Gambar</label>
                              <input type="file" name="gambar">
                            
                            </div>
                         
                                 <input type="hidden"  name="status_masakan" value="Y" id="exampleInputPassword1">

                            
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"> Simpan</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



<?php
include "footer_admin.php";
?>