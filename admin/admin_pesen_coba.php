<?php

include "header_admin.php";
include 'database.php';
$db = new database();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-6">
        <div class="box">

          <div class="box-header">

            <h3 class="box-title">Data Menu Makanan</h3>

          </div><!-- /.box-header -->
          <div class="box-body">
             <?php
         include "../login/koneksi.php";
                                    // Include / load file koneksi.php
                                    // Cek apakah terdapat data pada page URL
                                    $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                                    $limit = 3; // Jumlah data per halamanya

                                    // Buat query untuk menampilkan daa ke berapa yang akan ditampilkan pada tabel yang ada di database
                                    $limit_start = ($page - 1) * $limit;

                                    // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
                                    $data1=mysqli_query($conn, "SELECT * FROM masakan where status_masakan='Y'LIMIT ".$limit_start.",".$limit);
                                    $no = $limit_start + 1; // Untuk penomoran tabel
                                    while($data=mysqli_fetch_array($data1)){
                                        $harga= $data['harga'];
              $hasil="Rp".number_format($harga,2,',','.');
                                    ?>
        
              <div class="col-sm-6 col-md-4">

                <div class="thumbnail">
                  <h5><p align="center"><?php echo $data['nama_masakan'];?></p></h5>
                  <img src="../images/<?php echo $data['gambar'];?>" height='100' width='120'>
                  <div class="caption">
                    <p align="center"><?php echo $hasil;?></p>
                    <p align="center"> <a href="cart.php?act=add&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=pesan_masakan.php">Order Now</a></p>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>
                 
          </div>
          <div class="pagination">
               <?php
            if ($page == 1) { // Jika page adalah pake ke 1, maka disable link PREV
            ?>
                <li class="disabled"><a href="#">First</a></li>
                <li class="disabled"><a href="#">&laquo;</a></li>
            <?php
            } else { // Jika buka page ke 1
                $link_prev = ($page > 1) ? $page - 1 : 1;
            ?>
                <li><a href="admin_pesen_coba.php?page=1">First</a></li>
                <li><a href="admin_pesen_coba.php?page=<?php echo $link_prev; ?>">&laquo;</a></li>
            <?php
            }
            ?>

            <!-- LINK NUMBER -->
            <?php
            // Buat query untuk menghitung semua jumlah data
            $sql2 = mysqli_query($conn,"SELECT COUNT(*) AS jumlah FROM masakan where status_masakan='Y' ");
            ($get_jumlah = (mysqli_fetch_array($sql2)));

            $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamanya
            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

            for ($i = $start_number; $i <= $end_number; $i++) {
                $link_active = ($page == $i) ? 'class="active"' : '';
            ?>
                <li <?php echo $link_active; ?>><a href="admin_pesen_coba.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
            }
            ?>

            <!-- LINK NEXT AND LAST -->
            <?php
            // Jika page sama dengan jumlah page, maka disable link NEXT nya
            // Artinya page tersebut adalah page terakhir
            if ($page == $jumlah_page) { // Jika page terakhir
            ?>
                <li class="disabled"><a href="#">&raquo;</a></li>
                <li class="disabled"><a href="#">Last</a></li>
            <?php
            } else { // Jika bukan page terakhir
                $link_next = ($page < $jumlah_page) ? $page + 1 : $jumlah_page;
            ?>
                <li><a href="admin_pesen_coba.php?page=<?php echo $link_next; ?>">&raquo;</a></li>
                <li><a href="admin_pesen_coba.php?page=<?php echo $jumlah_page; ?>">Last</a></li>
            <?php
            }
            ?>
            </div>

        </div>
      </div>

      <div class="col-xs-6">



        <div class="box">
          <div class="box-header">

            <h3 class="box-title">Data Pesanan</h3>
          </div><!-- /.box-header -->
          <div class="box-body">

           <table class="table table-bordered table-striped">

            <tr>
              <th><center>No</center></th>
              <th><center>Nama Masakan</center></th>
              <th><center>Harga</center></th>
              <th>Quantity</th>
              <th><center>Sub Total</center></th>
              <th><center>Calcel</center></th>
            </tr>
            <?php
            include "../login/koneksi.php";
        //MENAMPILKAN DETAIL KERANJANG BELANJA//
            $no = 1;
            $total = 0;
    //mysql_select_db($database_conn, $conn);
            if (isset($_SESSION['items'])) {
              foreach ($_SESSION['items'] as $key => $val) {
                $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
                $data = mysqli_fetch_array($query);
                $jumlah_barang = mysqli_num_rows($query);
                $jumlah_harga = $data['harga'] * $val;
                $total += $jumlah_harga;
                $hasil="Rp.".number_format($harga,2,',','.');
                $hasil1="Rp.".number_format($total,2,',','.');
                $hasil2="Rp.".number_format($jumlah_harga,2,',','.');
                ?>





                <tr>
                  <td><center><?php echo $no++; ?></center></td>
                  <td><center><?php echo $data['nama_masakan']; ?></center></td>
                  <td><center> <?php echo $hasil; ?></center></td>
                  <td ><center><button class="btn-btn-default"><a href="cart.php?act=min&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=pesan_masakan.php">
                    <i class="glyphicon glyphicon-minus"></i></a>
                  </button>
                    <?php echo ($val); ?> 
                  <button class="btn-btn-default"><a href="cart.php?act=plus&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=pesan_masakan.php">
                    <i class="glyphicon glyphicon-plus"></i></a>
                  </button></center></td>
                  <td><center><?php echo $hasil2 ?></center></td>
                  <td> <p align="center"> <a href="cart.php?act=del&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=pesan_masakan.php">Cancell</a></p></td>
                </tr>
                


                <?php
                    //mysql_free_result($query);      
              }
              //$total += $sub;
            }?>
            <?php
            if($total == 0){ ?>
            <td colspan="5" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
            <?php } else { ?>
            <td colspan="6" style="font-size: 18px;"><b><div class="pull-right">Total Harga  :<?php echo $hasil1; ?> </div> </b></td>

            <?php 

          }
          ?>



        </table>
        <p><div align="right">
          <a href="" class="btn btn-success">&raquo; Konfirmasi Pemesanan &laquo;</a>
        </div></p>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!-- /.col -->



</div>

</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php
include "footer_admin.php";
?>