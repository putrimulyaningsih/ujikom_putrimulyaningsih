<?php
include "header_admin.php";
?>
<?php 
include 'database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">



        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data User</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
          <table>
          <tr>
            <td width="110">
          <a href="#tambahuser" data-toggle="modal" class="btn btn-primary">Tambah Data</a></p>
           </td>
           <td>
            <a href="laporan_user.php" target="_blank" class="btn btn-primary">Export Data PDf</a></p>
          </td>
          </tr>
          </table>

             <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Username</th>
                <th>Password</th>
                <th>Nama User</th>
                <th>Level</th>
                <th>Email</th>
                       <th>Keterangan</th>
                <th>Aksi</th>
              </tr>
            </thead>
              <tbody>
                   <?php
                   error_reporting(0);
            $no = 1;
            foreach($db->tampil_data_user() as $x){
              ?>

                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['username']; ?></td>
                    <td><?php echo $x['password']; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                  <td><?php echo $x['nama_level']; ?></td>
                  <td><?php echo $x['email']; ?></td>
                         <td>
    
                        <?php
                                            if($x['keterangan'] == 'Y')
                                            {
                                              ?>
                                            <a href="approve.php?table=user&id_user=<?php echo $x['id_user']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                            Aktif
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="approve.php?table=user&id_user=<?php echo $x['id_user']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Non Aktif
                                            </a>
                                            <?php
                                            }
                                            ?>
                  </td>
                  <td>
                                   <a href="" data-toggle="modal" data-target="#myModaluser<?php echo $x['id_user'];?>" class="btn btn-warning">Edit</a> 
                  </td>
         

                </tr>

          

                 <?php 
            }
            ?>
              </tbody>
             
          </table>
        </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->


  </div><!-- /.row -->
  <!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php
include "../login/koneksi.php";
$no=0;
$data = "SELECT * from user";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
  ?>
  <?php
$id = $select_result['id_user']; 
$query_edit = mysqli_query($conn,"SELECT * FROM user WHERE id_user='$id'");
$r = mysqli_fetch_array($query_edit);
?>
            <div class="modal" id="myModaluser<?php echo $select_result['id_user'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Edit Menu</h4>
                  </div>


                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses_admin.php?aksi=update_user" enctype="multipart/form-data" class="form-horizontal form-material"">
                            <div class="form-group">
                                    <label for="username" class="col-md-4">Username :</label>
                                    <div class="col-md-12">
                                      <input type="hidden" name="id_user" value="<?php echo $r['id_user']?>">
                                      <input type="text" id="username" class="form-control" placeholder="Masukkan Username" name="username" value="<?php echo $r['username']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-4">Password :</label>
                                    <div class="col-md-12">
                                      <input type="password" name="password" id="password" class="form-control" placeholder="">
                                      <input type="hidden" name="password_lama" placeholder="" value="<?php echo $r['password'] ?>">   
                                      </div>                             
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-md-4">Email :</label>
                                    <div class="col-md-12">
                                      <input type="email" id="email" class="form-control" placeholder="Masukkan Email yang Falid" name="email" value="<?php echo $r['email']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_user" class="col-md-4">Nama User :</label>
                                    <div class="col-md-12">
                                      <input type="text" id="nama_user" class="form-control" placeholder="Masukkan Nama Anda" name="nama_user" value="<?php echo $r['nama_user']?>">
                                    </div>
                                </div>
                               <div class="form-group">
                                 <label for="id_level" class="col-md-4">Nama Level :</label>
                                 <div class="col-md-12">
                                    <select name="id_level" class="form-control col-md-12">
                                    <?php     
                                    include"../login/koneksi.php";
                                      $select=mysqli_query($conn, "SELECT * FROM level");
                                      while($show=mysqli_fetch_array($select)){
                                    ?>
                                      <option value="<?=$show['id_level'];?>" <?=$r['id_level']==$show['id_level']?'selected':null?>><?=$show['nama_level'];?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                </div>
                      <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <?php
          }
          ?>
<div class="modal" id="tambahuser">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Tambah User</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses_admin.php?&aksi=input_user" enctype="multipart/form-data" class="form-horizontal form-material"">
                        <div class="box-body">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Usename</label>
                              <input type="text" class="form-control" name="username" placeholder="Masukan Usename" required>
                            </div>
                             
                            <div class="form-group">
                              <label for="exampleInputPassword1">Password</label>
                              <input type="password" class="form-control" name="password"  placeholder="Masukan Password" required>
                            </div>
                           <div class="form-group">
                              <label for="exampleInputPassword1">Nama Lengkap</label>
                              <input type="integer" class="form-control" name="nama_user"  placeholder="Masukan Nama" required>
                            </div>
                            <div class="form-group">
                                    <label for="id_level">Nama Level :</label>
                                    <select name="id_level" class="form-control" required>
                                       <option>Pilih Nama Level</option>
                                          <?php     
                                            include"../login/koneksi.php";
                                            $select=mysqli_query($conn, "SELECT * FROM level");
                                            while($show=mysqli_fetch_array($select)){
                                          ?>
                                      <option value="<?=$show['id_level'];?>"><?=$show['nama_level'];?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                             <div class="form-group">
                              <label for="exampleInputPassword1">E-mail</label>
                              <input type="email" class="form-control" name="email"  placeholder="Masukan E-mail" required>
                            </div>
                                  <input type="hidden"  name="keterangan"  value="Y">
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"> Simpan</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

<?php
include "footer_admin.php";
?>