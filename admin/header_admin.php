<?php
session_start();

if($_SESSION['id_level']==""){
  header("location:../login/login.php");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../water/index_water.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/dashboard.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/master_owner.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/menu.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Admin Planet Restaurant</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
 
    <!-- Bootstrap 3.3.4 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>PR</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</b> Planet Resto</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
     
           
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php  echo $_SESSION['username'];?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                    <p>
                   <?php  echo $_SESSION['username'];?>
                    
                    </p>
                  </li>
                  <!-- Menu Body -->
              
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="../login/logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
           
            <div class="pull-left info">
              
            </div>
          </div>
          <!-- search form -->
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
         
          
            <li class="active treeview">
              <a href="admin_master.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
              </a>
            
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Master Data</span>
                <span class="label label-primary pull-right"></span>
              </a>
              <ul class="treeview-menu">
                <li><a href="masakan.php"><i class="fa fa-circle-o"></i> Data Masakan </a></li>
                <li><a href="data_user.php"><i class="fa fa-circle-o"></i> Data User</a></li>
                <li><a href="data_meja.php"><i class="fa fa-circle-o"></i> Data Meja</a></li>
                <li><a href="data_kategori.php"><i class="fa fa-circle-o"></i> Data Kategori</a></li>

              
              </ul>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Orderan</span>
              </a>
              <ul class="treeview-menu">
               <li><a href="data_pesanan.php"><i class="fa fa-circle-o"></i> Data Orderan </a>
             
           </li>
                <li><a href="daftar_meja.php"><i class="fa fa-circle-o"></i> Pesan Orderan</a></li>
             </ul>
            </li>
           
            <li class="treeview">
             <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Transaksi</span>
              </a>
             <ul class="treeview-menu">
             <li>
              <a href="transaksi_order.php">
                <i class="fa fa-pie-chart"></i>
                Transaksi
              </a>
              </li>
               <li>
              <a href="laporan_transaksi.php">
                <i class="fa fa-pie-chart"></i>
               Laporan Transaksi
              </a>
              </li>
             
            </li>
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>