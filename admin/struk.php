<?php
include "header_admin.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
    <div class="content">
 <div class="col-xs-12">

        <div class="box">
        <div class="content">
         <?php 
if(isset($_GET['pesan'])){
  $pesan=$_GET['pesan'];
  if($pesan=="berhasil"){
    echo "<div class='alert alert-success'>Transaksi Berhasil Dilakukan</div>";
  }else if($pesan=="gagal"){
    echo "<div class='alert alert-danger'>Uang yang Anda Masukan Kuran dari jumlah Pembayaran  !! Silahkan Isi Kembali !! </div>";
  }
}
?>

        <div class="container">
          <?php
          include "../login/koneksi.php";
          $get_id_odr=$_GET['id_order'];
          $query=mysqli_query($conn,"SELECT id_order from oder where id_order='$get_id_odr'");
$lihat_id=mysqli_fetch_array($query);
$id_order=  $lihat_id['id_order'];

        ?>
          <table>
          <tr>
            <td width="80">
          <a href="transaksi_order.php" data-toggle="modal" class="btn btn-primary">Kembali</a></p>
           </td>
           <td>
            <a href="cetak_struk.php?id_order=<?php echo $id_order;?>" target="_blank" class="btn btn-primary">Cetak Struk</a></p>
          </td>
          </tr>
          </table>
       
</div>
            <h3 align="center"><b> Struk Planet Restaurant</b></h3>
          <div class="box-body">
       <table class="table table-bordered table-striped">
              <thead>
      
                <tr>
                  <th>No</th>
                  <th>Nama Masakan</th>
                  <th>Harga</th>
                    <th>Quantity</th>
                   <th>Sub Harga</th>
                 
                   
                 

                </tr>
                
              </thead>
        
                <tbody>
        <?php
        include "../login/koneksi.php";
              $no=1;
$get_id=$_GET['id_order'];
$query=mysqli_query($conn,"SELECT * from oder inner join detail_order on oder.id_order=detail_order.id_order inner join masakan on detail_order.id_masakan=masakan.id_masakan inner join transaksi on oder.id_order=transaksi.id_order where oder.id_order='$get_id'");
while($lihat=mysqli_fetch_array($query)){
	 $jumlah=$lihat['jumlah']*$lihat['harga'];
	$harga=$lihat['harga'];
	$hasil="Rp".number_format($harga,2,',','.');
	$hasil_kali="Rp".number_format($jumlah,2,',','.');
    $total_bayar=$lihat['total_bayar'];
    $total_bayar_format="Rp".number_format($total_bayar,2,',','.');
    $jumlah_uang=$lihat['jumlah_uang'];
    $jumlah_uang_format="Rp".number_format($jumlah_uang,2,',','.');
    $kembalian=$lihat['kembalian'];
    $kembalian_format="Rp".number_format($kembalian,2,',','.');

      ?>
                  <tr>
                    <td><?php echo $no++;?></td>
                    <td><?php echo $lihat['nama_masakan'];?></td>
                   
                    <td><?php echo $hasil;?></td>
                     <td><?php echo $lihat['jumlah'];?></td>
                    <td><?php echo $hasil_kali;?></td>

                  </tr>
                 

              
               
              <tr>
                <?php

}
?>
              
  <td colspan="4" align="right"><h4><b>Total Pembayaran</b></h4></td>
  <td ><h4><?php echo $total_bayar_format;?></h4></td>
 
  </tr>
   <tr>
  <td colspan="4" align="right"><h4><b>Jumlah Uang</b></h4></td>
  <td ><h4><?php echo $jumlah_uang_format;?></h4></td>
 
  </tr>
   <tr>
  <td colspan="4" align="right"><h4><b>Kembalian</b></h4></td>
  <td ><h4><?php echo $kembalian_format;?></h4></td>
 
  </tr>

  </tbody>
            </table>

            
 
                 
                </div>
              </div>
            
           
    </div>
    </section>
    </div>
    </div>
    <?php
include "footer_admin.php";
?>