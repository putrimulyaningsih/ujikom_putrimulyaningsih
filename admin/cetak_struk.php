<?php
include '../login/koneksi.php';
require('../assets/fpdf.php');

$pdf = new FPDF("P","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(8); 
$pdf->SetFont('Arial','B',14);           
$pdf->MultiCell(10,0.5,'Struk Planet Restaurant',0,'P');
$pdf->SetFont('Arial','B',10);
$pdf->SetX(9);
$pdf->MultiCell(10,0.5,'JL. Ciherang Cutak',0,'P');
$pdf->SetX(6);
$pdf->MultiCell(10,0.5,'website : www.planet.com email : planet@gmail.com',0,'P');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Tanggal : ".date("D-d/m/Y"),0,0,'C');
$pdf->Cell(19,0.7,"Kasir : ".date("D-d/m/Y"),0,0,'C');

$pdf->ln(1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Nama Masakan', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Harga Masakan', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Kategori Masakan', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Kategori Masakan', 1, 1, 'C');


$pdf->SetFont('Arial','',10);
$no=1;
$get_id=$_GET['id_order'];
$query=mysqli_query($conn,"SELECT * from oder inner join detail_order on oder.id_order=detail_order.id_order inner join masakan on detail_order.id_masakan=masakan.id_masakan where oder.id_order='$get_id'");
while($lihat=mysqli_fetch_array($query)){
	 $jumlah=$lihat['jumlah']*$lihat['harga'];
	$harga=$lihat['harga'];
	 $hasil="Rp".number_format($harga,2,',','.');
	  $hasil_kali="Rp".number_format($jumlah,2,',','.');
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['nama_masakan'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $hasil, 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['jumlah'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $hasil_kali,1, 1, 'C');


	$no++;
}
$query1=mysqli_query($conn,"SELECT * from transaksi where id_order='$get_id'");
$lihat_id=mysqli_fetch_array($query1);
	$harga=$lihat_id['total_bayar'];
	 $hasil="Rp.".number_format($harga,2,',','.');
	 $harga1=$lihat_id['jumlah_uang'];
	 $hasil1="Rp.".number_format($harga1,2,',','.');
	 $harga2=$lihat_id['kembalian'];
	 $hasil2="Rp.".number_format($harga2,2,',','.');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->SetX(8);
$pdf->Cell(16,0.7,"Total Pembayaran  :           ".$hasil,0,0,'C');

$pdf->ln(1);
$pdf->SetX(8);
$pdf->Cell(16.5,0.7,"Jumlah Uang      :          ".$hasil1,0,0,'C');

$pdf->ln(1);
$pdf->SetX(8);
$pdf->Cell(15.5,0.7,"Jumlah Pengembalian :         ".$hasil2,0,0,'C');









$pdf->Output("struk.pdf","I");

?>

