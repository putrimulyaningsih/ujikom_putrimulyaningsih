<?php
include "header_admin.php";
?>
<?php 
include 'database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">
                                   



        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Meja</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="agile3-grids">
              <p align="left"><a href="#tambahmeja" data-toggle="modal" class="btn btn-primary">Tambah Data</a></p>
            </div>
             <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Meja</th>
                  <th>Status Meja</th>
                </tr>
              </thead>
              <tbody>
               <?php
               error_reporting(0);
               $no = 1;
               foreach($db->data_meja() as $x){
                ?>

                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['no_meja']; ?></td>
                  <td>

                    <?php
                    if($x['status_meja'] == 'Y')
                    {
                      ?>
                      <?php echo "tersedia";?>

                      <?php
                    }else{
                      ?>
                      <?php echo " Penuh";?>
                      <?php 
                    }
                    ?>
                  </td>
                  

                </tr>



                <?php 
              }
              ?>
            </tbody>

          </table>
        </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->


  </div><!-- /.row -->
  <!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->

  <div class="modal" id="tambahmeja" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Form Tambah Meja</h4>
        </div>


        <div class="modal-body">
          <form role="form"  method="POST" action="proses_admin.php?aksi=tambah_meja" enctype="multipart/form-data" class="form-horizontal form-material">
           
            <div class="form-group">
              <label for="no_meja" class="col-md-4">No Meja :</label>
              <div class="col-md-12">
                <input type="text" id="no_meja" class="form-control" placeholder="Masukkan No meja" name="no_meja" >
              </div>
            </div>
            
          
            <div class="modal-footer">
              <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div><!-- /.box-body -->
          </form>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->



<?php
include "footer_admin.php";
?>