<?php 

class database{

	var $host = "localhost";
	var $uname = "root";
	var $pass = "";
	var $db = "restauran_putri";
	public $mysqli;

	function __construct(){
		$this->mysqli = new mysqli($this->host, $this->uname, $this->pass, $this->db);
	}

	function tampil_data(){
		$data =$this->mysqli->query("SELECT * from masakan inner join kategori on masakan.id_kategori=kategori.id_kategori order by id_masakan DESC ");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function data_meja(){
		$data =$this->mysqli->query("SELECT * from meja  ");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
		function data_kategori(){
		$data =$this->mysqli->query("SELECT * from kategori  ");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_data_user(){
		$data =$this->mysqli->query("SELECT * from user INNER JOIN level ON user.id_level = level.id_level  order by id_user DESC");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

		function tampil_data_user_kasir(){
		$data =$this->mysqli->query("SELECT * from user INNER JOIN level ON user.id_level = level.id_level where  nama_level='pelanggan' order by id_user DESC");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function tampil_data_user_water(){
		$data =$this->mysqli->query("SELECT * from user INNER JOIN level ON user.id_level = level.id_level where  nama_level='pelanggan' order by id_user DESC");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
		function pesan_tampil(){
		$data =$this->mysqli->query("SELECT oder.id_order, oder.no_meja, oder.tanggal, user.nama_user,oder.keterangan,oder.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from oder INNER JOIN user ON oder.id_user = user.id_user inner join transaksi on oder.id_order=transaksi.id_order");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
    		function pesan_tampil_tanggal(){
		$data =$this->mysqli->query("SELECT oder.id_order, oder.no_meja, oder.tanggal, user.nama_user,oder.keterangan,oder.status_order,transaksi.keterangan_transaksi,transaksi.id_transaksi from oder INNER JOIN user ON oder.id_user = user.id_user inner join transaksi on oder.id_order=transaksi.id_order where oder.tanggal BETWEEN '$_POST[tgl1]' and '$_POST[tgl2]' ");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
		function tampilan_water(){
		$data =$this->mysqli->query("SELECT oder.id_order, oder.no_meja, oder.tanggal, user.nama_user,oder.keterangan,oder.status_order,transaksi.keterangan_transaksi, transaksi.id_transaksi, transaksi.total_bayar from oder INNER JOIN user ON oder.id_user = user.id_user inner join transaksi on oder.id_order=transaksi.id_order where transaksi.keterangan_transaksi='N'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function detail_tampil(){

 
		$data =$this->mysqli->query("SELECT * from detail_order INNER JOIN masakan ON masakan.id_masakan= detail_order.id_masakan where detail_order.id_order='$_GET[id_order]'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	function hapus_ma($id_masakan){
		$data = $this->mysqli->query("DELETE from masakan where id_masakan='$id_masakan'");
	}
function hapus_u($id_user){
		$data = $this->mysqli->query("DELETE from user where id_user='$id_user'");
	}
	function input_u($username,$password,$nama_user,$id_level,$email,$keterangan){
		$data =$this->mysqli->query("INSERT into user values('','$username','$password','$nama_user','$id_level','$email','$keterangan')");
	}	
		function pesan_tampil_keterangan(){
		$data =$this->mysqli->query("SELECT oder.id_order, oder.no_meja, oder.tanggal, user.nama_user,oder.keterangan,oder.status_order,transaksi.keterangan_transaksi, transaksi.id_transaksi, transaksi.total_bayar, user.id_user from oder INNER JOIN user ON oder.id_user = user.id_user inner join transaksi on oder.id_order=transaksi.id_order where transaksi.keterangan_transaksi='N' and oder.status_order='Y'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}

	function data_pesan_tampil(){
		$data =$this->mysqli->query("SELECT * from masakan where status_masakan='Y'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
    	function tampil_data_transaksi(){
		$data =$this->mysqli->query("SELECT * from transaksi inner join user on transaksi.id_user=user.id_user where transaksi.keterangan_transaksi='Y'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
	 function update_user($id_user,$username,$password_aman,$email,$nama_user,$id_level){
		$data = $this->mysqli->query("update user set username='$username', password='$password_aman', email='$email', nama_user='$nama_user', id_level='$id_level' where id_user='$id_user'");
	}
	function input_meja($no_meja,$status_meja){
		$data =$this->mysqli->query("INSERT into meja values('','$no_meja','$status_meja')");
	}
		function input_kategori($nama_kategori){
		$data =$this->mysqli->query("INSERT into kategori values('','$nama_kategori')");
	}
	
} 

?>