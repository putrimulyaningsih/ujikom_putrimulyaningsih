<?php
include "header_admin.php";
?>
<?php 
include 'database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">
                                   



        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Meja</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="agile3-grids">
              <p align="left"><a href="#tambahmeja" data-toggle="modal" class="btn btn-primary">Tambah Data</a></p>
            </div>
             <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <td align="center"><b>No</b></td>
                  <td align="center"><b>Nama Kategori</b></td>


                </tr>
              </thead>
              <tbody>
               <?php
               error_reporting(0);
               $no = 1;
               foreach($db->data_kategori() as $x){
                ?>

                <tr>
                  <td align="center"><?php echo $no++; ?></td>
                  <td align="center"><?php echo $x['nama_kategori']; ?></td>
       
                  

                </tr>



                <?php 
              }
              ?>
            </tbody>

          </table>
        </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->


  </div><!-- /.row -->
  <!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->

  <div class="modal" id="tambahmeja" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Form Tambah Kategori</h4>
        </div>


        <div class="modal-body">
          <form role="form"  method="POST" action="proses_admin.php?aksi=tambah_kategori" enctype="multipart/form-data" class="form-horizontal form-material">
           
            <div class="form-group">
              <label for="nama_kategori" class="col-md-4">Nama Kategori :</label>
              <div class="col-md-12">
                <input type="text" id="nama_kategori" class="form-control" placeholder="Masukkan Nama Kategori" name="nama_kategori" >
              </div>
            </div>
            
          
            <div class="modal-footer">
              <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div><!-- /.box-body -->
          </form>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->



<?php
include "footer_admin.php";
?>