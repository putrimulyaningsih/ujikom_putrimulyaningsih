<?php
include "header_admin.php";
?>
<?php 
include 'database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
       <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
             <?php
             include "../login/koneksi.php";
$query_edit = mysqli_query($conn,"SELECT *,oder.tanggal FROM oder inner join user on oder.id_user=user.id_user inner join transaksi on oder.id_order=transaksi.id_order where oder.id_order='$_GET[id_order]'");
$x = mysqli_fetch_array($query_edit)
?>
                  <h3 class="box-title">Data Order NO Meja <?php echo $x['no_meja']; ?></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">No Meja</label>
                      <input class="form-control" value="<?php echo $x['no_meja']; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Pembeli</label>
                      <input  class="form-control" value="<?php echo $x['nama_user']; ?>" readonly>
                    </div>
                         <div class="form-group">
                      <label for="exampleInputEmail1">Tanggal</label>
                      <input class="form-control" value="<?php echo $x['tanggal']; ?>" readonly>
                    </div>
                            <div class="form-group">
                      <label for="exampleInputEmail1">Keterangan Pembayaran</label>
                    <?php
                                            if($x['keterangan_transaksi'] == 'Y')
                                            {
                                              ?>
                                         
                      <input class="form-control" value="<?php echo "Terbayar"; ?>" readonly>
                    
                                          
                                            <?php
                                            }else{
                                              ?>
                                           
                     
                      <input class="form-control" value="<?php echo "Belum Terbayar"; ?>" readonly>
                    </div>
                                            <?php 
            }
            ?>
                  </div><!-- /.box-body -->
                </form>
            
              </div><!-- /.box -->
            </div>

      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            
            <h3 class="box-title">Data Orderan</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            
             <div class="agile3-grids">
            <p align="left"><a href="tambah_pesanan.php?id_order=<?php echo $_GET['id_order'] ?>" class="btn btn-success">Tambah Pesanan</a></p>
          </div>
          <div class="table-responsive">
       <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Masakan</th>
                  <th>Harga</th>
                    <th>Quantity</th>
                   <th>Keterangan</th>
                   <th>Status</th>
                   <th>Jumlah</th>
                   
                 

                </tr>
              </thead>
              <?php
              error_reporting(0);
              $no = 1;
              $total=0;
             
              foreach($db->detail_tampil() as $x){
                $harga=$x['harga'];
                $jumlah=$x['jumlah']*$x['harga'];
                  $hasil="Rp.".number_format($harga,2,',','.');
            
                    $jumlah1="Rp.".number_format($jumlah,2,',','.');
                  
                ?>
                <tbody>

                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $x['nama_masakan']; ?></td>
                    <td><?php echo $hasil ; ?></td>
                     <td><?php echo $x['jumlah']; ?></td>
                    <td><?php echo $x['keterangan']; ?></td>
                    <td><?php echo $x['status_detail_order']; ?></td>
                 <td><?php echo $jumlah1; ?></td>
                    

                  </tr>
                 

                </tbody>
                <?php 
                $total += ($jumlah) ;
                $total1="Rp.".number_format($total,2,',','.');
              }
              ?>
              <tr>
  <td colspan="6" align="right"><h4><b>Total</b></h4></td>
  <td ><h4><?php echo $total1;?></h4></td>
 
  </tr>
            </table>
          </div>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
        


    </div><!-- /.row -->
    <!-- Main row -->


  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include "footer_admin.php";
?>