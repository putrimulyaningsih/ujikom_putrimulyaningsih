<?php

include "header_water.php";

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">

          <div class="box-header">

            <h3 class="box-title">Data Menu Makanan</h3>

          </div><!-- /.box-header -->
          <div class="box-body">
             <?php
         include "../login/koneksi.php";
                                    // Include / load file koneksi.php
                                    // Cek apakah terdapat data pada page URL
                                    $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                                    $limit = 6; // Jumlah data per halamanya

                                    // Buat query untuk menampilkan daa ke berapa yang akan ditampilkan pada tabel yang ada di database
                                    $limit_start = ($page - 1) * $limit;

                                    // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
                                    $data1=mysqli_query($conn, "SELECT * FROM masakan where status_masakan='Y'LIMIT ".$limit_start.",".$limit);
                                    $no = $limit_start + 1; // Untuk penomoran tabel
                                    while($data=mysqli_fetch_array($data1)){
                                        $harga= $data['harga'];
              $hasil="Rp.".number_format($harga,2,',','.');
                                    ?>
        
              <div class="col-sm-6 col-md-2">

                <div class="thumbnail">
                  <h5><p align="center"><input type="hidden"  class="validate"><?php echo $data['nama_masakan'];?></p></h5>
                <a href ="cart_water.php?act=add&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=tambah_pesanan_water.php?id_order=<?php echo $_GET['id_order'];?>"> <img src="../images/<?php echo $data['gambar'];?>" height='100' width='120'></a>
                  <div class="caption">
                    <p align="center"><?php echo $hasil;?></p>
                
                  </div>
                </div>
              </div>
              <?php
            }
            ?>
                 
          </div><div class="container">
          <div class="pagination">
               <?php
            if ($page == 1) { // Jika page adalah pake ke 1, maka disable link PREV
            ?>
                <li class="disabled"><a href="#">First</a></li>
                <li class="disabled"><a href="#">&laquo;</a></li>
            <?php
            } else { // Jika buka page ke 1
                $link_prev = ($page > 1) ? $page - 1 : 1;
            ?>
                <li><a href="tambah_pesanan_water.php?id_order=<?php echo $_GET['id_order'];?>?page=1">First</a></li>
                <li><a href="tambah_pesanan_water.php?id_order=<?php echo $_GET['id_order'];?>?page=<?php echo $link_prev; ?>">&laquo;</a></li>
            <?php
            }
            ?>

            <!-- LINK NUMBER -->
            <?php
            // Buat query untuk menghitung semua jumlah data
            $sql2 = mysqli_query($conn,"SELECT COUNT(*) AS jumlah FROM masakan where status_masakan='Y' ");
            ($get_jumlah = (mysqli_fetch_array($sql2)));

            $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamanya
            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

            for ($i = $start_number; $i <= $end_number; $i++) {
                $link_active = ($page == $i) ? 'class="active"' : '';
            ?>
                <li <?php echo $link_active; ?>><a href="tambah_pesanan_water.php?id_order=<?php echo $_GET['id_order'];?>?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
            }
            ?>

            <!-- LINK NEXT AND LAST -->
            <?php
            // Jika page sama dengan jumlah page, maka disable link NEXT nya
            // Artinya page tersebut adalah page terakhir
            if ($page == $jumlah_page) { // Jika page terakhir
            ?>
                <li class="disabled"><a href="#">&raquo;</a></li>
                <li class="disabled"><a href="#">Last</a></li>
            <?php
            } else { // Jika bukan page terakhir
                $link_next = ($page < $jumlah_page) ? $page + 1 : $jumlah_page;
            ?>
                <li><a href="tambah_pesanan_water.php?id_order=<?php echo $_GET['id_order'];?>?page=<?php echo $link_next; ?>">&raquo;</a></li>
                <li><a href="tambah_pesanan_water.php?id_order=<?php echo $_GET['id_order'];?>?page=<?php echo $jumlah_page; ?>">Last</a></li>
            <?php
            }
            ?>
            </div></div>

        </div>
      </div>

      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">

            <h3 class="box-title">Data Pesanan</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
<form action="proses_penjualan_water.php?id_order=<?php echo $_GET['id_order']?>" method="post">
    
        <table class="table table-bordered table-striped">
          <div class="container">
          <button type="submit" class="btn btn-success">Simpan</button></div></br>
            <tr>    
               <th><center>No</center></th>
                    <th><center>Nama Masakan</center></th>
                    <th><center>Harga</center></th>
                    <th><center>Quantity</center></th>

                    <th><center>Sub Total</center></th>

                    <th><center>Keterangan</center></th>
                    <th><center>Cancel</center></th>
              
            </tr>
        <?php
        //MENAMPILKAN DETAIL KERANJANG BELANJA//
    $no = 1;
    $total = 0;
    //mysql_select_db($database_conn, $conn);
    if (isset($_SESSION['items'])) {
        foreach ($_SESSION['items'] as $key => $val) {
            $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
            $data = mysqli_fetch_array($query);
      $jumlah_barang = mysqli_num_rows($query);
            $jumlah_harga = $data['harga'] * $val;
            $total += $jumlah_harga;
              $harga= $data['harga'];
              $hasil="Rp.".number_format($harga,2,',','.');
              $hasil1="Rp.".number_format($jumlah_harga,2,',','.');
               $total1="Rp.".number_format($total,2,',','.');
            ?>
              <tr>
                <input type="hidden" name='id_order[]' value="<?php echo $_GET['id_order']?>"/>

                <td><center><?php echo $no++; ?></center></td>


                <td><center><input type="hidden" name='id_masakan[]' value="<?php echo $data['id_masakan'];?>"><?php echo $data['nama_masakan']; ?></center></td>
                <td><center> <?php echo $hasil; ?> </center></td>
                <td><center> <a href="cart_water.php?act=plus&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=tambah_pesanan_water.php?id_order=<?php echo $_GET['id_order'];?>" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i></a>
                 <input type='hidden' name="jumlah[]" value="<?php echo ($val); ?>"><?php echo ($val); ?> Pcs <a href="cart_water.php?act=min&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=tambah_pesanan_water.php?id_order=<?php echo $_GET['id_order'];?>" class="btn btn-default"><i class="glyphicon glyphicon-minus"></i></a></center></td>
                <td><center><?php echo $hasil1; ?></center></td>
                 <td><center><textarea class="form-control" name="keterangan[]"></textarea></center></td>
                <td><center><a href="cart_water.php?act=del&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=tambah_pesanan_water.php?id_order=<?php echo $_GET['id_order'];?>">Cancel</a></center></td>
                </tr>

          <?php
                    //mysql_free_result($query);      
            }
              //$total += $sub;
            }?>
                        <?php
        if($total == 0){ ?>
          <td colspan="5" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
        <?php } else { ?>
                        <td colspan="7" style="font-size: 18px;"><b><div class="pull-right"><input type='hidden' value='<?php echo $total;?>' name='total_bayar' >Total Harga : <?php echo $total1; ?></div> </b></td>
          
        <?php 
        
        }
        ?>
      </table>
    </form>

      </div><!-- /.box-body -->
      
    </div><!-- /.box -->
  </div><!-- /.col -->
</section>



</div>

<?php
include "footer_water.php";
?>