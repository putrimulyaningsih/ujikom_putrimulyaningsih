<?php
include '../login/koneksi.php';
require('../assets/fpdf.php');

$pdf = new FPDF("P","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('../assets/malasngoding.png',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(10,0.5,'Planet Restaurant',0,'P');
$pdf->SetX(4);
$pdf->MultiCell(10,0.5,'Telpon : 0038XXXXXXX',0,'P');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(10,0.5,'JL. Ciherang Cutak',0,'P');
$pdf->SetX(4);
$pdf->MultiCell(10,0.5,'website : www.planet.com email : planet@gmail.com',0,'P');
$pdf->Line(1,3.1,20,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,20,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(18,0.7,"Laporan Menu Makanan",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'No Meja', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Nama User', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Keteranga Orderan', 1, 1, 'C');



$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($conn,"select *,oder.tanggal,oder.keterangan from oder inner join user on oder.id_user=user.id_user inner join transaksi on oder.id_order=transaksi.id_order where transaksi.keterangan_transaksi='N'");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['no_meja'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['nama_user'],1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['keterangan'], 1, 1,'C');
	



	$no++;
}

$pdf->Output("laporan_orderan_water.pdf","I");

?>

