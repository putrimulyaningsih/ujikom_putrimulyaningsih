<?php
include "header_water.php";
?>
<?php 
include '../admin/database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">



        <div class="box">
          <div class="box-header">

            <h3 class="box-title">Data Orderan</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
                     <?php 
if(isset($_GET['pesan'])){
  $pesan=$_GET['pesan'];
  if($pesan=="berhasil"){
    echo "<div class='alert alert-success'>Penerimaan Berhasil Dilakukan</div>";
  }else if($pesan=="gagal"){
    echo "<div class='alert alert-danger'>Penerimaan Gagal Dilakukan </div>";
  }
}
?>
<p align="left"><a href="laporan_orderan_water.php" target="_blank" class="btn btn-primary">Export Data PDf</a></p>
            <div class="table-responsive">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>NO Meja</th>
                <th>Tanggal</th>
                <th>Nama User</th>
                <th>Keterangn</th>
                <th>Status Order</th>
                <th>keterangan Pembayaran</th>
                <th>Aksi</th>

              </tr>
            </thead>
            
              <tbody>
                <?php
                error_reporting(0);
            $no = 1;
            foreach($db->tampilan_water() as $x){
              ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['no_meja']; ?></td>
                  <td><?php echo $x['tanggal']; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                  <td><?php echo $x['keterangan']; ?></td>
                        <td><?php
                                            if($x['status_order'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Sudah Diterima";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum Diterima";?>
                                            <?php 
            }
            ?></td>
                  <td><?php
                                            if($x['keterangan_transaksi'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Terbayar";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum Terbayar";?>
                                            <?php 
            }
            ?></td>
                  <td>
                    <a href="update_keterangan_ket.php?id_order=<?php echo $x['id_order']; ?>;"><button type="button" class="btn btn-success">Pesan</button></a>
                
                   <a href="detail_pesanan_water.php?id_order=<?php echo $x['id_order']; ?>;"><button type="button" class="btn btn-info">Detail</button></a>
                   
                 </td>


               </tr>
                <div class="modal" id="myModalu<?php echo $x['id_order'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Edit Menu</h4>
                  </div>
<?php
include '../login/koneksi.php';
$id = $x['id_order']; 
$query_edit = mysqli_query($conn,"SELECT oder.id_order,oder.no_meja,oder.tanggal,user.nama_user,oder.keterangan FROM oder inner join user on oder.id_user=user.id_user WHERE id_order='$id'");
$r = mysqli_fetch_array($query_edit);
?>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="../admin/update_order.php?id_order=<?php echo $r['id_order'];?>" enctype="multipart/form-data" class="form-horizontal form-material">
                             <div class="form-group">
                                    <label for="id_user">No Meja</label>
                                    <select name="no_meja" value="<?=$r['no_meja'];?>" class="form-control">
                                       <option><?php echo $r['no_meja'];?></option>
                                          <?php     
                                            include"../login/koneksi.php";
                                            $select=mysqli_query($conn, "SELECT * FROM meja");
                                            while($show=mysqli_fetch_array($select)){
                                          ?>
                                      <option value="<?=$show['id_meja'];?>"><?=$show['no_meja'];?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                            <div class="form-group">
                              <label for="tanggal">Tanggal</label>
                              <input type="date" class="form-control" name="tanggal" id="tanggal" value="<?php echo $r['tanggal'];?>" >
                            </div>
                              <div class="form-group">
                                    <label for="id_user">Nama User :</label>
                                    <select name="id_user" value="<?=$r['id_user'];?>" class="form-control">
                                       <option><?php echo $r['nama_user'];?></option>
                                          <?php     
                                            include"../login/koneksi.php";
                                            $select=mysqli_query($conn, "SELECT * FROM user");
                                            while($show=mysqli_fetch_array($select)){
                                          ?>
                                      <option value="<?=$show['id_user'];?>"><?=$show['nama_user'];?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                            <div class="form-group">
                              <label for="keterangan">Keterangan</label>
                              <input type="text"  class="form-control" name="keterangan" value="<?php echo $r['keterangan'];?>">
                            </div>

                                
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

                <?php 
           }
           ?>
             </tbody>
            
         </table>
       </div>
       </div><!-- /.box-body -->
     </div><!-- /.box -->
   </div><!-- /.col -->


 </div><!-- /.row -->
 <!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include "footer_water.php";
?>