<?php
include "header_water.php";
?>
<?php 
include '../admin/database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">



        <div class="box">

          <div class="box-header">
            <h3 class="box-title">Data User</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
                 <table><tr><td width="110">
           <a href="#tambahuser" data-toggle="modal" class="btn btn-primary">Tambah Data</a>
         </td>
       <td>
           <a href="laporan_user.php" target="_blank" class="btn btn-success">Export Data PDF</a>
         </td></tr></table>
       </br>
     
          <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Username</th>
                <th>Nama User</th>
                <th>Level</th>
                <th>Email</th>
                <th>Keterangan</th>
              </tr>
            </thead>
          
              <tbody>
                  <?php
            $no = 1;
            foreach($db->tampil_data_user_water() as $x){
              ?>

                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['username']; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                  <td><?php echo $x['nama_level']; ?></td>
                  <td><?php echo $x['email']; ?></td>
                     <td>
    
                        <?php
                                            if($x['keterangan'] == 'Y')
                                            {
                                              ?>
                                            <a href="approve.php?table=user&id_user=<?php echo $x['id_user']; ?>&action=not-verifed" class="btn btn-primary btn-md">
                                            Aktif
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="approve.php?table=user&id_user=<?php echo $x['id_user']; ?>&action=verifed" class="btn btn-danger btn-md">
                                            Non Aktif
                                            </a>
                                            <?php
                                            }
                                            ?>
                  </td>
                </tr>
                <?php
                                            }
                                            ?>
              </tbody>
            
          </table>
        </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->


  </div><!-- /.row -->
  <!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal" id="tambahuser">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Tambah User</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form"  method="POST" action="proses_water.php?&aksi=input_user" enctype="multipart/form-data" class="form-horizontal form-material"">
                        <div class="box-body">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Usename</label>
                              <input type="text" class="form-control" name="username" placeholder="Masukan Usename">
                            </div>
                             
                            <div class="form-group">
                              <label for="exampleInputPassword1">Password</label>
                              <input type="password" class="form-control" name="password"  placeholder="Masukan Password">
                            </div>
                           <div class="form-group">
                              <label for="exampleInputPassword1">Nama Lengkap</label>
                              <input type="text" class="form-control" name="nama_user"  placeholder="Masukan Nama">
                            </div>
                                <input type="hidden" class="form-control" name="id_level"  value="5">
                             <div class="form-group">
                              <label for="exampleInputPassword1">E-mail</label>
                              <input type="email" class="form-control" name="email"  placeholder="Masukan E-mail">
                            </div>
                             <input type="hidden" class="form-control" name="keterangan" value="Y" >
                                 
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"> Simpan</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

<?php
include "footer_water.php";
?>