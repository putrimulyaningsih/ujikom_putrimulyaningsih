<?php
include ('koneksi.php');
include ('function.php');

if(isset($_POST['submit']))
{
  $uemail = $_POST['email'];
  $uemail = mysqli_real_escape_string($conn, $uemail);
  
  if(checkUser($uemail) == "true")
  {
    $userID = UserID($uemail);
    $token = generateRandomString();
    
    $query = mysqli_query($conn, "INSERT INTO recovery_keys (userID, token) VALUES ($userID, '$token') ");
    if($query)
    {
     $send_mail = send_mail($uemail, $token);


     if($send_mail === 'success')
     {
       $msg = 'A mail with recovery instruction has sent to your email.';
       $msgclass = 'bg-success';
     }else{
      $msg = 'There is something wrong.';
      $msgclass = 'bg-danger';
    }



  }else
  {
    $msg = 'There is something wrong.';
    $msgclass = 'bg-danger';
  }

}else
{
  $msg = "This email doesn't exist in our database.";
  $msgclass = 'bg-danger';
}
}

?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>AdminLTE 2 | Log in</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.4 -->
  <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Font Awesome Icons -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
  <!-- iCheck -->
  <link href="../plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="login-page">
      <div class="login-box">
        <div class="login-logo">
          <a href="../index2.html"><b>Admin</b>LTE</a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
          
         
          <form action="" method="post">
             <?php if(isset($msg)) {?>
          <div class="<?php echo $msgclass; ?>" style="padding:5px;"><?php echo $msg; ?></div>
          <?php } ?>
          <br><br>
            <div class="form-group has-feedback">
              <input type="email" class="form-control" name="email" placeholder="Email"/>
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="row">
              <div class="col-xs-8">    
              </div><!-- /.col -->
              <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat" name="submit" >Sign In</button>
              </div><!-- /.col -->
            </div>
          </form>
                  <a href="login.php">Kembali</a><br>

         



        </div><!-- /.login-box-body -->
      </div><!-- /.login-box -->

      <!-- jQuery 2.1.3 -->
      <script src="../plugins/jQuery/jQuery-2.1.3.min.js"></script>
      <!-- Bootstrap 3.3.2 JS -->
      <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
      <!-- iCheck -->
      <script src="../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
      <script>
        $(function () {
          $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
        });
      </script>
    </body>
    </html>