<?php
session_start();

if($_SESSION['id_level']=="1"){
  header("location:../admin/admin_master.php");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../water/index_water.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/dashboard.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/master_owner.php");
}

elseif($_SESSION['id_level']==""){
  header("location:../login/login.php");
}
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Cooking School Free html template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <!--Google fonts Link-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i" rel="stylesheet">

        <link rel="stylesheet" href="../assets/css/skills/progressbar.css">
        <link rel="stylesheet" href="../assets/css/skills/style.css">
        <link rel="stylesheet" href="../assets/css/fonticons.css">
        <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="../assets/css/teamslide.css" />
        <link rel="stylesheet" href="../assets/css/plugins.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="../assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="../assets/css/responsive.css" />

        <script src="../assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body data-spy="scroll" data-target=".navbar-collapse">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <header id="main_menu" class="header navbar-fixed-top">            
            <div class="main_menu_bg">
                <div class="container">
                    <div class="row">
                        <div class="nave_menu">
                            <nav class="navbar navbar-default" id="navmenu">
                                <div class="container-fluid">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="#home">
                                            <img src="../images/baru.jpg"/>
                                           
                                        </a>

                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->



                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                                        <ul class="nav navbar-nav navbar-right">                   
                                            <li><a href="menu.php">Menu Makanan</a></li>
                                            <li><a href="logout.php">Logout</a></li>
                                               <li><a href="header.php#tambahuser" data-toggle="modal">
                                            <img src="../cart.png" width="30"> </a>
                                             
    </li>
               
                                        </ul>
                                    </div>

                                </div>
                            </nav>
                        </div>	
                    </div>

                </div>

            </div>
        </header> <!--End of header -->
      
                <div class="modal" id="tambahuser">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">List Pemesanan</h4>
                  </div>
                  <div class="modal-body">
                    

                            
                     <form action="proses_penjualan.php" method="post">
                            <div class="form-group">
                              <label>NO Meja</label>
                             

                              <div class="radio">
                                 <?php     
                                            include"../login/koneksi.php";
                                            $select=mysqli_query($conn, "SELECT * FROM meja where status_meja='Y'");
                                            while($show=mysqli_fetch_array($select)){
                                          ?>
                               
                                <label>
                                   
                                  <input type="radio" name="no_meja"  value="<?php echo $show['id_meja'];?>" >
                                <span> <?php echo $show['no_meja'];?></span>
                                 
                                </label>
                                <?php }?>
                                
                              </div>
                              
                              
                            </div>
                      <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <tr>   
               <th><center>No</center></th>
               <th><center>Nama Masakan</center></th>
               <th><center>Harga</center></th>
               <th><center>Quantity</center></th>

               <th><center>Sub Total</center></th>

               <th><center>Keterangan</center></th>
               <th><center>Cancel</center></th>

             </tr>
             <?php

             include '../login/koneksi.php';
                         $no = 1;
             $total = 0;
    //mysql_select_db($database_conn, $conn);
             if (isset($_SESSION['items'])) {
              foreach ($_SESSION['items'] as $key => $val) {
                $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
                $data = mysqli_fetch_array($query);
                $jumlah_barang = mysqli_num_rows($query);
                $jumlah_harga = $data['harga'] * $val;
                $total += $jumlah_harga;
                $harga= $data['harga'];
                $hasil="Rp.".number_format($harga,2,',','.');
                $hasil1="Rp.".number_format($jumlah_harga,2,',','.');
                $total1="Rp.".number_format($total,2,',','.');
                ?>
                <tr>

                  <td><center><?php echo $no++; ?></center></td>
                  <td><center><input type="hidden" name='id_masakan[]' value="<?php echo $data['id_masakan'];?>"><?php echo $data['nama_masakan']; ?></center></td>
                  <td><center> <?php echo $hasil; ?> </center></td>
                  <td><center><input type='hidden' name="jumlah[]" value="<?php echo ($val); ?>"><?php echo ($val); ?> Pcs </center></td>
                   <td><center><?php echo $hasil1; ?></center></td>
                   <td><center><textarea name="keterangan[]" ></textarea></center></td>
                   <td><center><a href="cart.php?act=del&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=menu.php">Cancel</a></center></td>
                 </tr>

                 <?php
                    //mysql_free_result($query);      
               }
              //$total += $sub;
             }?>
             <?php
             if($total == 0){ ?>
             <td colspan="7" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
             <?php } else { ?>
             <td colspan="7" style="font-size: 18px;"><b><div class="pull-right"><input type='hidden' value='<?php echo $total;?>' name='total_bayar' >Total Harga : <?php echo $total1; ?></div> </b></td>

             <?php 

           }
           ?>

         </table>
     </div>

                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                   <button type="submit" class="btn btn-primary">  Simpan</button>
                  </div><!-- /.box-body -->

                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

                            <div class="modal" id="tambahmeja">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">List Meja</h4>
                  </div>
                  <div class="modal-body">
                    
                        <div class="box-body">
                            
                     
                                 <div class="form-group">
                              <label>NO Meja</label>
                             

                              <div class="radio">
                                 <?php     
                                            include"../login/koneksi.php";
                                            $select=mysqli_query($conn, "SELECT * FROM meja where status_meja='Y'");
                                            while($show=mysqli_fetch_array($select)){
                                          ?>
                               
                                <label>
                                   
                                  <input type="radio" name="no_meja"  value="<?php echo $show['id_meja'];?>" >
                                <span> <?php echo $show['no_meja'];?></span>
                                 
                                </label>
                                <?php }?>
                                
                              </div>
                              
                              
                            </div>
                         
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"> Simpan</button>
                  </div><!-- /.box-body -->
   
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>






