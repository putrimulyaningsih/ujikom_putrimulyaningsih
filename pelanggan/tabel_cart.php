<?php
include 'header.php';
?>

<section id="portfolio" class="portfolio">
            <div class="container">
                <div class="row">
                    <div class="main_mix_content text-center sections">
                        <div class="head_title">
                            <h2>Daftar Pesanan</h2>
                        </div>
                           <p align='left'>
                <a href="menu.php"><button class="btn btn-success">Tambah Pesanan</button></a></p>
           <div class="table-responsive">
            <form action="proses_penjualan.php" method="post">
            <table class="table table-bordered table-striped">
                <tr>   
               <th><center>No</center></th>
               <th><center>Nama Masakan</center></th>
               <th><center>Harga</center></th>
               <th><center>Quantity</center></th>

               <th><center>Sub Total</center></th>

               <th><center>Keterangan</center></th>
               <th><center>Cancel</center></th>

             </tr>
             <?php

             include '../login/koneksi.php';
        //MENAMPILKAN DETAIL KERANJANG BELANJA//
             $no = 1;
             $total = 0;
    //mysql_select_db($database_conn, $conn);
             if (isset($_SESSION['items'])) {
              foreach ($_SESSION['items'] as $key => $val) {
                $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
                $data = mysqli_fetch_array($query);
                $jumlah_barang = mysqli_num_rows($query);
                $jumlah_harga = $data['harga'] * $val;
                $total += $jumlah_harga;
                $harga= $data['harga'];
                $hasil="Rp.".number_format($harga,2,',','.');
                $hasil1="Rp.".number_format($jumlah_harga,2,',','.');
                $total1="Rp.".number_format($total,2,',','.');
                ?>
                <tr>

                  <td><center><?php echo $no++; ?></center></td>
                  <td><center><input type="hidden" name='id_masakan[]' value="<?php echo $data['id_masakan'];?>"><?php echo $data['nama_masakan']; ?></center></td>
                  <td><center> <?php echo $hasil; ?> </center></td>
                  <td><center><input type='hidden' name="jumlah[]" value="<?php echo ($val); ?>"><?php echo ($val); ?> Pcs </center></td>
                   <td><center><?php echo $hasil1; ?></center></td>
                   <td><center><textarea name="keterangan[]" ></textarea></center></td>
                   <td><center><a href="cart.php?act=del&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=tabel_cart.php">Cancel</a></center></td>
                 </tr>

                 <?php
                    //mysql_free_result($query);      
               }
              //$total += $sub;
             }?>
             <?php
             if($total == 0){ ?>
             <td colspan="7" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
             <?php } else { ?>
             <td colspan="7" style="font-size: 18px;"><b><div class="pull-right"><input type='hidden' value='<?php echo $total;?>' name='total_bayar' >Total Harga : <?php echo $total1; ?></div> </b></td>

             <?php 

           }
           ?>
         </table>
       </div>
       <p align='right'>
             <a href="#tambahuser" data-toggle="modal" class="btn btn-primary">Pesan</a>
                <div class="modal" id="tambahuser">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Pelanggan</h4>
                  </div>
                  <div class="modal-body">
                    
                        <div class="box-body">
                            
                     
                                 <div class="form-group">
                              <label>NO Meja</label>
                             

                              <div class="radio">
                                 <?php     
                                            include"../login/koneksi.php";
                                            $select=mysqli_query($conn, "SELECT * FROM meja where status_meja='Y'");
                                            while($show=mysqli_fetch_array($select)){
                                          ?>
                               
                                <label>
                                   
                                  <input type="radio" name="no_meja"  value="<?php echo $show['id_meja'];?>" >
                                <span> <?php echo $show['no_meja'];?></span>
                                 
                                </label>
                                <?php }?>
                                
                              </div>
                              
                              
                            </div>
                         
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"> Simpan</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
     </form>
      

   </div><!-- /.box-body -->


    </div><!-- /.box -->
</div>
<?php
include 'footer.php';
?>