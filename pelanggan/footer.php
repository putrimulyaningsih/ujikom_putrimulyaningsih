<footer id="footer" class="footer">
    <div class="container">
        <div class="main_footer">
            <div class="row">

                <div class="col-sm-6 col-xs-12">
                    <div class="copyright_text">
                        <p class=" wow fadeInRight" data-wow-duration="1s">Made with <i class="fa fa-heart"></i> by <a href="https://bootstrapthemes.co">Bootstrap Themes</a>2016. All Rights Reserved</p>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12">
                    <div class="footer_socail">
                        <a href=""><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-twitter"></i></a>
                        <a href=""><i class="fa fa-google-plus"></i></a>
                        <a href=""><i class="fa fa-rss"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>



<!-- START SCROLL TO TOP  -->

<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div>

<script src="../assets/js/vendor/jquery-1.11.2.min.js"></script>
<script src="../assets/js/vendor/bootstrap.min.js"></script>

<script src="../assets/js/jquery.easypiechart.min.js"></script>
<script src="../assets/js/jquery.mixitup.min.js"></script>
<script src="../assets/js/jquery.easing.1.3.js"></script>
<script src="../assets/css/skills/inview.min.js"></script>
<script src="../assets/css/skills/progressbar.js"></script>
<script src="../assets/css/skills/main.js"></script>

<!--This is link only for gmaps-->
<script src="http://maps.google.com/maps/api/js"></script>
<script src="../assets/js/gmaps.min.js"></script>
<script>
    var map = new GMaps({
        el: '.ourmaps',
        scrollwheel: false,
        lat: -12.043333,
        lng: -77.028333
    });
</script>



<script src="../assets/js/plugins.js"></script>
<script src="../assets/js/main.js"></script>

</body>
</html>
