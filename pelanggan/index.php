<?php include 'header_awal_pelanggan.php';
?>
<section id="portfolio" class="portfolio">
            <div class="container">
                <div class="row">
                    <div class="main_mix_content text-center sections">
                        <div class="head_title">
                            <h2>MENU MAKANAN</h2>
                        </div>
                        <div class="main_mix_menu">
                            <ul>
                                    <li class="btn btn-primary filter" data-filter="all">Semua Menu</li>
                <?php
            include '../login/koneksi.php';
            $query_data_masakan= mysqli_query($conn,"SELECT * FROM kategori ");
            while($data = mysqli_fetch_array($query_data_masakan)){

             
            
              ?>
                                <li class="btn btn-primary filter" data-filter=".<?php echo $data['id_kategori'];?>"><?php echo $data['nama_kategori'];?></li>
                                         <?php
            }
            ?>

                            </ul>
                        </div>

                        <div id="mixcontent" class="mixcontent">
                            <?php
            include '../login/koneksi.php';
            $query_data_masakan= mysqli_query($conn,"SELECT * FROM masakan where status_masakan='Y'");
            while($data = mysqli_fetch_array($query_data_masakan)){
                $harga= $data['harga'];
                $hasil="Rp.".number_format($harga,2,',','.');
             
            
              ?>
                            
                            <div class="col-md-3 mix <?php echo $data['id_kategori'];?> no-padding">
                                <div class="single_mixi_portfolio">
                                    <img src="../images/<?php echo $data['gambar'];?>" alt="" />
                                    <div class="mixi_portfolio_overlay">
                                        <div class="overflow_hover_text">
                                            <h2><?php echo $data['nama_masakan'];?></h2>
                                            <p><?php echo $hasil;?></p>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
            }
            ?>

                            <div class="gap"></div>
                        </div>
                    </div>                     
                </div>
            </div>
        </section> <!-- End of portfolio two Section -->        
        <?php
        include 'footer.php';
        ?>


