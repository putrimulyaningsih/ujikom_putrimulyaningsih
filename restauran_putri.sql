-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2019 at 12:23 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restauran_putri`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE IF NOT EXISTS `detail_order` (
`id_detail_order` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_masakan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status_detail_order` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=519 ;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_masakan`, `jumlah`, `keterangan`, `status_detail_order`) VALUES
(516, 252, 22, 1, '', 'Y'),
(517, 253, 22, 1, 'jangan terlalu pedes yaa', 'Y'),
(518, 253, 22, 1, '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'makanan'),
(2, 'minuman'),
(3, 'minuman1');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
`id_level` int(11) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'administator'),
(2, 'water'),
(3, 'kasir'),
(4, 'owner'),
(5, 'pelanggan');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE IF NOT EXISTS `masakan` (
`id_masakan` int(11) NOT NULL,
  `nama_masakan` varchar(30) NOT NULL,
  `harga` char(35) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `status_masakan` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Tersedia, Y Tersedia',
  `gambar` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `nama_masakan`, `harga`, `id_kategori`, `status_masakan`, `gambar`) VALUES
(8, 'mulya', '12345', 0, 'N', 'avatar2.png'),
(9, 'jus jeruk', '2000', 0, 'N', 'avatar5.png'),
(10, 'mie ayam', '5435', 0, 'N', 'avatar5.png'),
(11, 'bakso', '2000', 0, 'N', 'avatar.png'),
(19, 'spageti', '2000', 0, 'N', 'avatar2.png'),
(20, 'mie ayam', '10000', 1, 'Y', 'gl2.jpg'),
(21, 'mie bakso', '20000', 1, 'N', 'fg2.jpg'),
(22, 'ayam bakar', '10000', 1, 'Y', 'pf3.jpg'),
(23, 'bakso', '20000', 2, 'Y', 'gl1.jpg'),
(24, 'ayam gebrek', '20000', 1, 'Y', 'fg7.jpg'),
(25, 'jus jambu', '2000', 2, 'Y', 'gl2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE IF NOT EXISTS `meja` (
`id_meja` int(11) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `status_meja` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`id_meja`, `no_meja`, `status_meja`) VALUES
(6, 1, 'N'),
(7, 2, 'N'),
(8, 3, 'N'),
(10, 4, 'N'),
(11, 5, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `oder`
--

CREATE TABLE IF NOT EXISTS `oder` (
`id_order` int(11) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status_order` varchar(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=254 ;

--
-- Dumping data for table `oder`
--

INSERT INTO `oder` (`id_order`, `no_meja`, `tanggal`, `id_user`, `keterangan`, `status_order`) VALUES
(252, 6, '2019-04-01', 35, '', 'Y'),
(253, 6, '2019-04-01', 35, '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `recovery_keys`
--

CREATE TABLE IF NOT EXISTS `recovery_keys` (
`rid` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `recovery_keys`
--

INSERT INTO `recovery_keys` (`rid`, `userID`, `token`, `valid`) VALUES
(1, 1, 'e9edf804323854b31d0fa9e61bbc0a35', 1),
(2, 1, 'c14f83d52893e3c374334e0338107310', 0),
(3, 1, 'b3b0dbead8ec929a0d937984bc25224b', 0),
(4, 1, 'caaaa13a7be5e8ed060671ded484aac1', 1),
(5, 1, '5b6afd124fb5cc442bbf772d7989b3e6', 0),
(6, 1, '3e41950962a1160107698f5af53a671e', 1),
(7, 1, 'edf75bb23449490d4bf7eaba469709e2', 0),
(8, 1, 'fa7eee0eb6972b167d156db2f1fb7f82', 1),
(9, 1, '15e65ca8f340c2c2ea35d23b3ec73b8f', 0),
(10, 6, 'ea1928318c7a003bf5cd01d24506a6fd', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
`id_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(30) NOT NULL,
  `keterangan_transaksi` varchar(1) NOT NULL,
  `jumlah_uang` int(11) NOT NULL,
  `kembalian` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=146 ;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`, `keterangan_transaksi`, `jumlah_uang`, `kembalian`) VALUES
(144, 36, 252, '2019-04-01', 10000, 'Y', 20000, 10000),
(145, 0, 253, '0000-00-00', 20000, 'N', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id_user` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(35) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `keterangan` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif, Y Aktif'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `email`, `keterangan`) VALUES
(6, 'admin', '4093fed663717c843bea100d17fb67c8', 'putri mulyaninsih', 1, 'putrimulyaningsih2906@gmail.com', 'Y'),
(19, 'water', '9460370bb0ca1c98a779b1bcc6861c2c', 'putri mulyaningsih', 2, 'putrimulyaningsih2906@gmail.com', 'Y'),
(35, 'pengguna', '8b097b8a86f9d6a991357d40d3d58634', 'putri mulyaninsihdjfghfdg', 5, 'putrimulyaningsih2906@gmail.com', 'N'),
(36, 'kasir', 'c7911af3adbd12a035b289556d96470a', 'yuliyah', 3, 'putrimulyaningsih2906@gmail.com', 'Y'),
(38, 'kitel', '8454c438e74098857cea56e7bd28025e', 'widiasarii09', 0, 'ma123@gmail.com', 'Y'),
(39, 'admin_put', '669060b93db4493cfe051e9b555b32bb', 'widiasarii09', 0, 'putrimulyaningsih2906@gmail.com', 'Y'),
(40, 'pengguna', '8b097b8a86f9d6a991357d40d3d58634', 'mulyaningsih', 5, 'putrimulyaningsih2906@gmail.com', 'Y'),
(41, 'putri', '4093fed663717c843bea100d17fb67c8', 'putri', 5, 'pelayan1@gmail.com', 'Y'),
(42, 'Widi', 'd49b9709c6ca14577f08b2ab21ab7964', 'widi', 5, 'fistalanid@gmail.com', 'Y'),
(43, 'owner', '72122ce96bfec66e2396d2e25225d70a', 'putri', 4, 'mulyaningsih@gmail.com', 'Y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
 ADD PRIMARY KEY (`id_detail_order`), ADD KEY `id_order` (`id_order`), ADD KEY `id_masakan` (`id_masakan`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
 ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `masakan`
--
ALTER TABLE `masakan`
 ADD PRIMARY KEY (`id_masakan`);

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
 ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `oder`
--
ALTER TABLE `oder`
 ADD PRIMARY KEY (`id_order`), ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
 ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
 ADD PRIMARY KEY (`id_transaksi`), ADD KEY `id_user` (`id_user`), ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id_user`), ADD KEY `id_level` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
MODIFY `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=519;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `masakan`
--
ALTER TABLE `masakan`
MODIFY `id_masakan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `meja`
--
ALTER TABLE `meja`
MODIFY `id_meja` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `oder`
--
ALTER TABLE `oder`
MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=254;
--
-- AUTO_INCREMENT for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=146;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
