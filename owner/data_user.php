<?php
include "header_owner.php";
?>
<?php 
include '../admin/database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">



        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data User</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
          <table>
          <tr>

           <td>
            <a href="laporan_user.php" target="_blank" class="btn btn-primary">Export Data PDf</a></p>
          </td>
          </tr>
          </table>

             <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Username</th>
                <th>Password</th>
                <th>Nama User</th>
                <th>Level</th>
                <th>Email</th>
                       <th>Keterangan</th>
 
              </tr>
            </thead>
              <tbody>
                   <?php
                   error_reporting(0);
            $no = 1;
            foreach($db->tampil_data_user() as $x){
              ?>

                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['username']; ?></td>
                    <td><?php echo $x['password']; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                  <td><?php echo $x['nama_level']; ?></td>
                  <td><?php echo $x['email']; ?></td>
                         <td>
    
                        <?php
                                            if($x['keterangan'] == 'Y')
                                            {
                                              ?>

                                            Aktif
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                            
                                            Non Aktif
                                            </a>
                                            <?php
                                            }
                                            ?>
                  </td>
                
                </tr>

          

                 <?php 
            }
            ?>
              </tbody>
             
          </table>
        </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->


  </div><!-- /.row -->
  <!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->


<?php
include "footer_owner.php";
?>