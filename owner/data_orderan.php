<?php
include "header_owner.php";
?>
<?php 
include '../admin/database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">



        <div class="box">
          
          <div class="box-header">

            <h3 class="box-title">Data Orderan Terbayar</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <form method="POST" action="data_orderan_pertgl.php">
            Mulai tanggal <input type="date" name="tgl1"/>
            akhir tanggal<input type="date" name="tgl2"/>
            <input type="submit" value="cari">
            </form>
            </br>
            <p align="left"><a href="laporan_orderan.php" target="_blank" class="btn btn-primary">Export Data PDf</a></p>
        
           <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>NO Meja</th>
                <th>Tanggal</th>
                <th>Nama User</th>
                <th>Keterangn</th>
                <th>Status Order</th>
                 <th>Keterangan Pembayaran</th>
                <th>Aksi</th>

              </tr>
            </thead>
           
              <tbody>
                 <?php
                 error_reporting(0);
            $no = 1;
            foreach($db->pesan_tampil() as $x){
              ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['no_meja']; ?></td>
                  <td><?php echo $x['tanggal']; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                  <td><?php echo $x['keterangan']; ?></td>
                <td><?php
                                            if($x['status_order'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Sudah Diterima";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum Diterima";?>
                                            <?php 
            }
            ?></td>
                    <td><?php
                                            if($x['keterangan_transaksi'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Terbayar";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum Terbayar";?>
                                            <?php 
            }
            ?></td>
                  <td>

                   <a href="detail_pesanan.php?id_order=<?php echo $x['id_order']; ?>;"><button type="button" class="btn btn-success">Detail</button></a>

                   
                 </td>


               </tr>
              
                  <?php 
           }
           ?>
             </tbody>
          
         </table>
       </div><!-- /.box-body -->
     </div><!-- /.box -->
   </div><!-- /.col -->


 </div><!-- /.row -->
 <!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include "footer_owner.php";
?>