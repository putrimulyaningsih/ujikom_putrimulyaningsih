<?php
include "header_owner.php";
?>
<?php 
include '../admin/database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">



        <div class="box">
          <div class="box-header">

            <h3 class="box-title">Data Menu Makanan</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
          <table>
          <tr>

            <td>
            <p align="left"><a href="laporan_masakan.php" target="_blank" class="btn btn-primary">Export Data PDf</a></p>
            </td>
            </tr>
            </table>
        
           <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Masakan</th>
                  <th>Harga Masakan</th>
                  <th>Gambar Masakan</th>
                  <th>Kategori Masakan</th>
                   <th>Status Masakan</th>
      

                </tr>
              </thead>
             
                <tbody>
                   <?php
              $no = 1;
              foreach($db->tampil_data() as $x){
                $harga= $x['harga'];
                $hasil="Rp".number_format($harga,2,',','.');
             
                ?>

                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $x['nama_masakan']; ?></td>

                    <td><?php echo $hasil; ?></td>
                     <td><img src="../images/<?php echo $x['gambar']; ?>" height='100'></td>
                      <td><?php echo $x['nama_kategori'];?></td>
                                <td>
    
                        <?php
                                            if($x['status_masakan'] == 'Y')
                                            {
                                              ?>
                                          
                                            Tersedia
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                 
                                            Tidak Tersedia
                                            </a>
                                            <?php
                                            }
                                            ?>
                  </td>
                  
                  </tr>
                  
                      <?php 
              }
              ?>
                </tbody>
            
            </table>
             <div class="table-responsive">
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->


    </div><!-- /.row -->
    <!-- Main row -->


  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
 
<?php
include "footer_owner.php";
?>