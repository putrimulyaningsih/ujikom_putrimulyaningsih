<?php
include '../login/koneksi.php';
require('../assets/fpdf.php');

$pdf = new FPDF("P","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('../assets/malasngoding.png',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(10,0.5,'Planet Restaurant',0,'P');
$pdf->SetX(4);
$pdf->MultiCell(10,0.5,'Telpon : 0038XXXXXXX',0,'P');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(10,0.5,'JL. Ciherang Cutak',0,'P');
$pdf->SetX(4);
$pdf->MultiCell(10,0.5,'website : www.planet.com email : planet@gmail.com',0,'P');
$pdf->Line(1,3.1,20,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,20,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(18,0.7,"Laporan Menu Makanan",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(5, 0.8, 'Nama Masakan', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Harga Masakan', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Kategori Masakan', 1, 0, 'C');
$pdf->Cell(3.5, 0.8, 'Status Masakan', 1, 1, 'C');



$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($conn,"select * from masakan inner join kategori on masakan.id_kategori=kategori.id_kategori");
while($lihat=mysqli_fetch_array($query)){
	$harga=$lihat['harga'];
	 $hasil="Rp".number_format($harga,2,',','.');
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(5, 0.8, $lihat['nama_masakan'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $hasil, 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['nama_kategori'],1, 0, 'C');

	if($lihat['status_masakan']='Y'){
	$pdf->Cell(3.5, 0.8, 'Tersedia', 1, 1,'C');
}else{
	$pdf->Cell(3.5, 0.8, 'Tidak Tersedia', 1, 1,'C');

}


	$no++;
}

$pdf->Output("laporan_menu.pdf","I");

?>

