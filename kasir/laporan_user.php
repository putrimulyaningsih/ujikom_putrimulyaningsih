<?php
include '../login/koneksi.php';
require('../assets/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('../assets/malasngoding.png',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'Planet Restaurant',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 0038XXXXXXX',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. Ciherang Cutak',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.planet.com email : planet@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data User",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Username', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Nama User', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Level', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Email', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Keterangan', 1, 1, 'C');


$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($conn,"SELECT * from user inner join level on user.id_level=level.id_level where user.id_level='5'");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['username'],1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['nama_user'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['nama_level'],1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['email'], 1, 0,'C');
	if($lihat['keterangan']='Y'){
	$pdf->Cell(3, 0.8, 'Aktif', 1, 1,'C');
}else{
	$pdf->Cell(3, 0.8, 'Tidak Aktif', 1, 1,'C');

}


	$no++;
}

$pdf->Output("laporan_user.pdf","I");

?>

