<?php
include "header_kasir.php";
?>
<?php 
include '../admin/database.php';
$db = new database();
?>
<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-xs-12">



        <div class="box">
          <div class="box-header">

            <h3 class="box-title">Data Orderan</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
           <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>NO Meja</th>
                <th>Tanggal</th>
                <th>Nama User</th>
                <th>Keterangn</th>
                <th>Status Order</th>
                <th>Aksi</th>

              </tr>
            </thead>
            <?php
            $no = 1;
            foreach($db->pesan_tampil() as $x){
              ?>
              <tbody>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['no_meja']; ?></td>
                  <td><?php echo $x['tanggal']; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                  <td><?php echo $x['keterangan']; ?></td>
                  <td><?php echo $x['status_order']; ?></td>
                  <td>
                   <a href="proses_admin.php?id_user=<?php echo $x['id_user']; ?>&aksi=hapus_user";><button type="button" class="btn btn-warning">Edit</button></a>
                   <a href="proses_admin.php?id_order=<?php echo $x['id_order']; ?>&aksi=hapus_user" onclick="return confirm('Apakah Yakin Dihapus');"><button type="button" class="btn btn-danger">Hapus</button></a>
                   <a href="detail_pesanan.php?id_order=<?php echo $x['id_order']; ?>;"><button type="button" class="btn btn-info">Detail</button></a>
                 </td>


               </tr>
             </tbody>
             <?php 
           }
           ?>
         </table>
       </div><!-- /.box-body -->
     </div><!-- /.box -->
   </div><!-- /.col -->


 </div><!-- /.row -->
 <!-- Main row -->


</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include "footer_kasir.php";
?>